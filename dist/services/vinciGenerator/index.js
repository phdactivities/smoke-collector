"use strict";

var _require = require('./utils/checkSchemas'),
    validateSchema = _require.validateSchema;

module.exports.validateSchema = validateSchema;